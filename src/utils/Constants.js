export const OS_TYPE_ANDROID = 'android';
export const OS_TYPE_IOS = 'ios';
export const SHOW_AFTER = 200;
// All Screens Name
export const SCREEN_ALBUM_LIST = 'ssAlbumList';
export const SCREEN_FAVORITE_LIST = 'ssFavoriteList';


export const KEY_HEADERS = "headers";
export const KEY_BODY = "body";
export const KEY_TOKEN = "token";

export const KEY_VALUE = 'key';
export const KEY_ITEM_NAME = 'name';
export const KEY_TYPE_NEXT = 'next';
export const KEY_TYPE_DONE = 'done';

//CustomAsyncStorage
export const ALBUM_LIST_DATA = 'ALBUM_LIST_DATA';


//Api item data
export const ITEM_IM_IMAGE = "im:image";
export const ITEM_IM_NAME = "im:name";
export const ITEM_IM_ARTIST = "im:artist";
export const ITEM_IM_TITLE = "title";
export const ITEM_IM_PRICE = "im:price";
export const ITEM_IM_RELEASE_DATE = "im:releaseDate";
export const ITEM_IM_RIGHTS = "rights";
