import React, { Component } from 'react';
import {
  View, TouchableOpacity, BackHandler, Dimensions, FlatList, Image
} from 'react-native';
import * as Colors from '../common/Colors';
import * as StringKeys from '../common/StringKeys';
import * as Strings from '../common/Strings';
import * as Dimens from '../common/Dimens';
import CommonHeader from '../common/CommonHeader';
import CommonText from '../common/CommonText';
import BaseComponent from '../common/BaseComponent';
import * as Constants from '../../utils/Constants';

import CustomPBar from '../common/CustomPBar';
import { SearchBar } from 'react-native-elements';
import PopupDialog from 'react-native-popup-dialog';

const { height, width } = Dimensions.get('window')

// Key value store system. It should be used instead of Local Storage.
import * as CustomAsyncStorage from '../../utils/CustomAsyncStorage';

//Below three imports are using for api calling
import * as types from '../../actions/types';
import { connect } from 'react-redux';
import { getAlmubListApi } from '../../actions';

import _ from 'lodash';

class AlbumList extends BaseComponent {

  constructor(props) {
    super(props);
    this.handleBackButtonClick = this.handleBackButtonClick.bind(this);

    this.state = {
      name: 'praveen singh',
      search: '',
      dataAlbumArray: [],
      isdetailDialogVisible: false,
    }
    this.albumListData = [];
    this.selectedItem = undefined
  }

  componentDidMount() {
    BackHandler.addEventListener('hardwareBackPress', this.handleBackButtonClick);
    this.callAlmubListApi();
  }

  componentWillUnmount() {
    BackHandler.removeEventListener('hardwareBackPress', this.handleBackButtonClick);
  }

  //Api Response(instead of i can use as a function handleResponse(nextProps){})
  componentWillReceiveProps(nextProps) {

    if (nextProps != undefined &&
      nextProps.currentScreen != undefined &&
      nextProps.currentScreen === Constants.SCREEN_ALBUM_LIST) {

      if (nextProps.showBar != undefined) {
        this.setState({ showProgress: nextProps.showBar })
      }

      if (nextProps.response != undefined &&
        nextProps.response != null && nextProps.response != '') {

        this.setState({
          showProgress: false
        })

        if (nextProps.type === types.API_ALBUM_LIST) {

          if (nextProps.response != undefined &&
            nextProps.response.feed != undefined &&
            nextProps.response.feed.entry != undefined) {

            this.showToast('Success')
            let newArray = nextProps.response.feed.entry;
            let updatedNewArray = [];
            newArray.forEach(element => {
              element['is_checked'] = false,
                updatedNewArray.push(element);
            });
            this.setState({
              showProgress: false,
              dataAlbumArray: updatedNewArray
            });
            this.albumListData = updatedNewArray;
          } else {
            this.showToast('Error in response')
          }
        }
      }
    }
  }

  //Handle back button
  handleBackButtonClick = () => {
    BackHandler.exitApp();
    return true;
  }

  //Calling api here
  callAlmubListApi = () => {

    this.props.getAlmubListApi({
      username: this.state.name,
    }, Constants.SCREEN_ALBUM_LIST)
  }

  search = text => {
    console.log(text);
  };

  //Clear search
  clear = () => {
    this.search.clear();
  };

  SearchFilterFunction(text) {
    //passing the inserted text in textinput
    const newData = this.albumListData.filter(function (item) {
      //applying filter for the inserted text in search bar
      const itemData = item[Constants.ITEM_IM_TITLE].label ? item[Constants.ITEM_IM_TITLE].label.toUpperCase() : ''.toUpperCase();
      const textData = text.toUpperCase();
      return itemData.indexOf(textData) > -1;
    });
    this.setState({
      //setting the filtered newData on datasource
      dataAlbumArray: newData,
      search: text,
    });
  }

  activeUnactivePress = (item, index) => {

    let albumArray = this.state.dataAlbumArray;
    let element = albumArray[index];
    element["is_selected"] = !item.is_selected
    this.setState({
      dataAlbumArray: albumArray
    })
  }

  handleRightClick = () => {

    let albumArray = this.state.dataAlbumArray;
    let updatedAlbumArray = [];
    albumArray.forEach(element => {
      if (element['is_selected']) {
        updatedAlbumArray.push(element);
      }
    });

    this.props.navigation.navigate(Constants.SCREEN_FAVORITE_LIST,
      {
        "dataAlbumArrayList": {
          favoriteAlbumArray: updatedAlbumArray
        }
      });
  }

  render() {
    return (
      <View style={style.containerMain}>

        {/* Heder view */}
        <CommonHeader
          alignSelf='flex-start'
          headerBg={Colors.colorPrimaryDark}
          backgroundColor={Colors.colorPrimaryDark}
          headerTitle={this.strings(StringKeys.Heading_App_Name)}
          rightICImagePath={require('../../../assets/images/like_active.png')}
          rightIconPress={this.handleRightClick}
          textAlign={'center'}
          textLangAlign={'center'}
          marginRightLeft={Dimens.px_5}
        />

        {/* Search bar */}
        <SearchBar
          round
          searchIcon={{ size: 24 }}
          onChangeText={text => this.SearchFilterFunction(text)}
          onClear={text => this.SearchFilterFunction('')}
          placeholder={this.strings(StringKeys.Text_Search_Here)}
          value={this.state.search}
        />

        <View style={{
          flex: 1,
          flexDirection: "column",
          padding: Dimens.px_5
        }}>
          <FlatList
            showsVerticalScrollIndicator={false}
            data={_.isUndefined(this.state.dataAlbumArray) && _.isNull(this.state.dataAlbumArray) ?
              [] : this.state.dataAlbumArray}
            renderItem={({ item, index }) => this.renderAlbumItem(item, index)}
            keyExtractor={(item, index) => index.toString()}
            extraData={this.state}
          />

        </View>
        <CustomPBar showProgress={this.state.showProgress} />
        <PopupDialog
          dialogStyle={{
            justifyContent: 'center',
            elevation: Dimens.px_5,
            borderRadius: Dimens.px_10,
            borderColor: Colors.placeholderTextColorExtraLight,
            borderWidth: Dimens.px_1,
          }}
          visible={this.state.isdetailDialogVisible}
          width={.9}
          height={.3}
          ref={(popupDialog) => {
            this.popupDialog = popupDialog;
          }}
          onTouchOutside={() => {
            this.setState({
              isdetailDialogVisible: false
            })
          }}>

          {/* Dialog View with image, title, artist, album price, release date and rights */}
          <View
            style={{
              flexDirection: 'row',
              backgroundColor: Colors.white,
              alignItems: 'center',
            }}>
            <Image

              source={(_.isUndefined(this.selectedItem)) ?
                require('../../../assets/images/like_active.png') :
                { uri: this.selectedItem[Constants.ITEM_IM_IMAGE][0].label }}
              style={{
                flex: .25,
                height: '100%',
              }}
              resizeMode={'contain'}
            />
            {/* Dialog Top(Right View) */}
            <View
              style={{
                flexDirection: 'column',
                padding: Dimens.px_5,
                flex: .75,
              }}>
              <CommonText
                fontFamily={Strings.font_medium}
                marginTop={Dimens.px_2}
                title={(_.isUndefined(this.selectedItem)) ? '' :
                  this.selectedItem[Constants.ITEM_IM_TITLE].label}
                numberOfLines={Dimens.px_3}
                fontSize={Dimens.txt_size_medium}
                color={Colors.placeholderTextColorDark}
              />

              <CommonText
                fontFamily={Strings.font_medium}
                marginTop={Dimens.px_5}
                title={(_.isUndefined(this.selectedItem)) ? '' :
                  this.strings(StringKeys.Text_Artist) + ' ' + this.selectedItem[Constants.ITEM_IM_ARTIST].label}
                numberOfLines={Dimens.px_3}
                fontSize={Dimens.txt_size_small}
                color={Colors.placeholderTextColorDark}
              />

              <CommonText
                fontFamily={Strings.font_medium}
                marginTop={Dimens.px_2}
                title={(_.isUndefined(this.selectedItem)) ? '' :
                  this.strings(StringKeys.Text_Price) + ' ' + this.selectedItem[Constants.ITEM_IM_PRICE].label}
                numberOfLines={Dimens.px_1}
                fontSize={Dimens.txt_size_small}
                color={Colors.placeholderTextColorDark}
              />
              <CommonText
                fontFamily={Strings.font_regular}
                marginTop={Dimens.px_2}
                title={(_.isUndefined(this.selectedItem)) ? '' :
                  this.strings(StringKeys.Text_Release_Date) + ' ' + this.selectedItem[Constants.ITEM_IM_RELEASE_DATE].attributes.label}
                numberOfLines={Dimens.px_1}
                fontSize={Dimens.txt_size_small}
                color={Colors.placeholderTextColorLight}
              />

              <CommonText
                fontFamily={Strings.font_regular}
                marginTop={Dimens.px_2}
                title={(_.isUndefined(this.selectedItem)) ? '' :
                  this.selectedItem[Constants.ITEM_IM_RIGHTS].label}
                numberOfLines={Dimens.px_1}
                fontSize={Dimens.txt_size_small_small}
                color={Colors.placeholderTextColorLight}
              />
            </View>
          </View>
        </PopupDialog>
      </View >
    );
  }

  // Flat list item(Album list item)
  renderAlbumItem(item, index) {
    return (
      <TouchableOpacity
        activeOpacity={.8}
        style={style.itemMain}
        onPress={() => {
          this.selectedItem = item;
          this.setState({
            isdetailDialogVisible: true
          })
        }}>
        <View
          style={{
            width: '20%',
            padding: Dimens.px_10,
            justifyContent: 'flex-start',
          }}>
          <Image
            source={{
              uri: item[Constants.ITEM_IM_IMAGE][0].label,
              cache: 'only-if-cached'
            }}
            style={{
              width: Dimens.px_50,
              height: Dimens.px_50
            }}
            resizeMode={'contain'}
          />
        </View>
        <View
          style={{
            width: '80%',
            paddingRight: Dimens.px_10,
            paddingVertical: Dimens.px_10,
            flexDirection: 'column',
            justifyContent: 'center'
          }}>
          <CommonText
            fontFamily={Strings.font_medium}
            marginTop={Dimens.px_2}
            title={item[Constants.ITEM_IM_NAME].label}
            numberOfLines={Dimens.px_1}
            fontSize={Dimens.txt_size_medium}
            color={Colors.white}
          />

          <CommonText
            fontFamily={Strings.font_medium}
            marginTop={Dimens.px_2}
            title={item[Constants.ITEM_IM_ARTIST].label}
            numberOfLines={Dimens.px_2}
            fontSize={Dimens.txt_size_small}
            color={Colors.white}
          />
          {item.is_selected == true ?
            <TouchableOpacity
              activeOpacity={.8}
              onPress={() => {
                this.activeUnactivePress(item, index);
              }}
              style={{
                alignSelf: 'flex-end',
                marginTop: Dimens.px_2,
              }}>
              <Image
                source={require('../../../assets/images/like_active.png')}
                style={{
                  width: Dimens.px_25,
                  height: Dimens.px_25,

                }}
                resizeMode={'contain'}
              />
            </TouchableOpacity> :
            <TouchableOpacity
              activeOpacity={.8}
              onPress={() => {
                this.activeUnactivePress(item, index);
              }}
              style={{
                alignSelf: 'flex-end',
                marginTop: Dimens.px_2,
              }}>
              <Image
                source={require('../../../assets/images/like_unactive.png')}
                style={{
                  width: Dimens.px_25,
                  height: Dimens.px_25,
                }}
                resizeMode={'contain'}
              />
            </TouchableOpacity>}
        </View>
      </TouchableOpacity>
    );
  }
}

const style = {

  containerMain:
  {
    flex: 1,
    flexDirection: 'column',
    backgroundColor: Colors.colorPrimary
  },
  itemMain: {
    width: '100%',
    flexDirection: 'row',
    backgroundColor: Colors.colorAccent,
    marginTop: Dimens.px_5,
    alignItems: 'center'
  },
}

function mapStateToProps({ response }) {
  return response;
}

export default connect(mapStateToProps, { getAlmubListApi })(AlbumList);
