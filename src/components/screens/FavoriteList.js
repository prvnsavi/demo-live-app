import React, { Component } from 'react';
import {
  View, TouchableOpacity, BackHandler, FlatList, Image
} from 'react-native';
import * as Colors from '../common/Colors';
import * as StringKeys from '../common/StringKeys';
import * as Strings from '../common/Strings';
import * as Dimens from '../common/Dimens';
import CommonText from '../common/CommonText';
import CommonHeader from '../common/CommonHeader';
import BaseComponent from '../common/BaseComponent';
import * as Constants from '../../utils/Constants';

import CustomPBar from '../common/CustomPBar';
import PopupDialog from 'react-native-popup-dialog';

import _ from 'lodash';

class FavoriteList extends BaseComponent {

  constructor(props) {
    super(props);
    this.handleBackButtonClick = this.handleBackButtonClick.bind(this);

    this.state = {
      dataAlbumArray: [],
      isdetailDialogVisible: false,
    }
  }

  componentDidMount() {
    BackHandler.addEventListener('hardwareBackPress', this.handleBackButtonClick);
    this.setState({
      dataAlbumArray: this.props.navigation.state.params.dataAlbumArrayList.favoriteAlbumArray
    })
  }

  componentWillUnmount() {
    BackHandler.removeEventListener('hardwareBackPress', this.handleBackButtonClick);
  }

  //Handle back button
  handleBackButtonClick = () => {
    this.props.navigation.goBack();
    return true;
  }

  render() {
    return (
      <View style={style.containerMain}>

        {/* Header view */}
        <CommonHeader
          alignSelf='flex-start'
          headerBg={Colors.colorPrimaryDark}
          backgroundColor={Colors.colorPrimaryDark}
          headerTitle={this.strings(StringKeys.Heading_Favorite)}
          leftICImagePath={require('../../../assets/images/backarrow.png')}
          leftIconPress={this.handleBackButtonClick}
          textAlign={'center'}
          textLangAlign={'center'}
          marginRightLeft={Dimens.px_5}
        />

        {_.isEmpty(this.state.dataAlbumArray) ?
          <View
            style={{
              flex: 1,
              flexDirection: 'column',
              width: '100%',
              alignItems: 'center',
              justifyContent: 'center'
            }}>
            <CommonText
              fontFamily={Strings.font_regular}
              title={"ʕ•ᴥ•ʔ"}
              fontSize={Dimens.txt_size_large}
              color={Colors.placeholderTextColorDark} />
            <CommonText
              fontFamily={Strings.font_regular}
              title={this.strings(StringKeys.Text_Favorite_List_Not_Available)}
              fontSize={Dimens.txt_size_large}
              color={Colors.placeholderTextColorDark} />
          </View> :
          <View style={{
            flex: 1,
            flexDirection: "column",
            padding: Dimens.px_5
          }}>
            <FlatList
              showsVerticalScrollIndicator={false}
              data={_.isUndefined(this.state.dataAlbumArray) && _.isNull(this.state.dataAlbumArray) ?
                [] : this.state.dataAlbumArray}
              renderItem={({ item, index }) => this.renderAlbumItem(item, index)}
              keyExtractor={(item, index) => index.toString()}
              extraData={this.state}
            />
          </View>}
        <CustomPBar showProgress={this.state.showProgress} />
        <PopupDialog
          dialogStyle={{
            justifyContent: 'center',
            elevation: Dimens.px_5,
            borderRadius: Dimens.px_10,
            borderColor: Colors.placeholderTextColorExtraLight,
            borderWidth: Dimens.px_1,
          }}
          visible={this.state.isdetailDialogVisible}
          width={.9}
          height={.3}
          ref={(popupDialog) => {
            this.popupDialog = popupDialog;
          }}
          onTouchOutside={() => {
            this.setState({
              isdetailDialogVisible: false
            })
          }}>

          {/* Dialog View with image, title, artist, album price, release date and rights */}
          <View
            style={{
              flexDirection: 'row',
              backgroundColor: Colors.white,
              alignItems: 'center',
            }}>
            <Image

              source={(_.isUndefined(this.selectedItem)) ?
                require('../../../assets/images/like_active.png') :
                { uri: this.selectedItem[Constants.ITEM_IM_IMAGE][0].label }}
              style={{
                flex: .25,
                height: '100%',
              }}
              resizeMode={'contain'}
            />
            {/* Dialog Top(Right View) */}
            <View
              style={{
                flexDirection: 'column',
                padding: Dimens.px_5,
                flex: .75,
              }}>
              <CommonText
                fontFamily={Strings.font_medium}
                marginTop={Dimens.px_2}
                title={(_.isUndefined(this.selectedItem)) ? '' :
                  this.selectedItem[Constants.ITEM_IM_TITLE].label}
                numberOfLines={Dimens.px_3}
                fontSize={Dimens.txt_size_medium}
                color={Colors.placeholderTextColorDark}
              />

              <CommonText
                fontFamily={Strings.font_medium}
                marginTop={Dimens.px_5}
                title={(_.isUndefined(this.selectedItem)) ? '' :
                  this.strings(StringKeys.Text_Artist) + ' ' + this.selectedItem[Constants.ITEM_IM_ARTIST].label}
                numberOfLines={Dimens.px_3}
                fontSize={Dimens.txt_size_small}
                color={Colors.placeholderTextColorDark}
              />

              <CommonText
                fontFamily={Strings.font_medium}
                marginTop={Dimens.px_2}
                title={(_.isUndefined(this.selectedItem)) ? '' :
                  this.strings(StringKeys.Text_Price) + ' ' + this.selectedItem[Constants.ITEM_IM_PRICE].label}
                numberOfLines={Dimens.px_1}
                fontSize={Dimens.txt_size_small}
                color={Colors.placeholderTextColorDark}
              />
              <CommonText
                fontFamily={Strings.font_regular}
                marginTop={Dimens.px_2}
                title={(_.isUndefined(this.selectedItem)) ? '' :
                  this.strings(StringKeys.Text_Release_Date) + ' ' + this.selectedItem[Constants.ITEM_IM_RELEASE_DATE].attributes.label}
                numberOfLines={Dimens.px_1}
                fontSize={Dimens.txt_size_small}
                color={Colors.placeholderTextColorLight}
              />

              <CommonText
                fontFamily={Strings.font_regular}
                marginTop={Dimens.px_2}
                title={(_.isUndefined(this.selectedItem)) ? '' :
                  this.selectedItem[Constants.ITEM_IM_RIGHTS].label}
                numberOfLines={Dimens.px_1}
                fontSize={Dimens.txt_size_small_small}
                color={Colors.placeholderTextColorLight}
              />
            </View>
          </View>
        </PopupDialog>
      </View >
    );
  }

  // Flat list item(Album list item)
  renderAlbumItem(item, index) {
    return (
      <TouchableOpacity
        activeOpacity={.8}
        style={style.itemMain}
        onPress={() => {
          this.selectedItem = item;
          this.setState({
            isdetailDialogVisible: true
          })
        }}>
        <View
          style={{
            width: '20%',
            padding: Dimens.px_10,
            justifyContent: 'flex-start',
          }}>
          <Image
            source={{
              uri: item[Constants.ITEM_IM_IMAGE][0].label,
              cache: 'only-if-cached'
            }}
            style={{
              width: Dimens.px_50,
              height: Dimens.px_50
            }}
            resizeMode={'contain'}
          />
        </View>
        <View
          style={{
            width: '80%',
            paddingRight: Dimens.px_10,
            paddingVertical: Dimens.px_10,
            flexDirection: 'column',
            justifyContent: 'center'
          }}>
          <CommonText
            fontFamily={Strings.font_medium}
            marginTop={Dimens.px_2}
            title={item[Constants.ITEM_IM_NAME].label}
            numberOfLines={Dimens.px_1}
            fontSize={Dimens.txt_size_medium}
            color={Colors.white}
          />

          <CommonText
            fontFamily={Strings.font_medium}
            marginTop={Dimens.px_2}
            title={item[Constants.ITEM_IM_ARTIST].label}
            numberOfLines={Dimens.px_2}
            fontSize={Dimens.txt_size_small}
            color={Colors.white}
          />
        </View>
      </TouchableOpacity>
    );
  }
}

const style = {

  containerMain:
  {
    flex: 1,
    flexDirection: 'column',
    backgroundColor: Colors.colorPrimary
  },
  itemMain: {
    width: '100%',
    flexDirection: 'row',
    backgroundColor: Colors.colorAccent,
    marginTop: Dimens.px_5,
    alignItems: 'center'
  },
}

export default FavoriteList;
