import React, { Component } from 'react';
import { strings } from '../i18n/i18n';

import Toast from 'react-native-simple-toast';
class Basecomponents extends Component {

  constructor(props) {
    super(props);
    this.myRef = React.createRef();
  }

  handleBackButtonClick = () => {
    console.log('backButton call Basecomponents' + JSON.stringify(this.props));
    this.props.goBack();

    return true;
  }

  strings(name, params = {}) {
    return strings(name, params);
  };

  showToast = (message) => {

    setTimeout(() => { Toast.show(message, Toast.CENTER) }, 200)

  }

}

export default Basecomponents;
