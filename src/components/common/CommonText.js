import React, { Component } from 'react';
import { View, TextInput, Text } from 'react-native';
import * as Dimens from './Dimens';
import * as Colors from './Colors';
import { Text_Current_Job_Is_Successfully_Completed } from './StringKeys';

//var width = Dimensions.get('window').width;

class CommonText extends Component {
  state = { btnBgColor: Colors.blue };

  async componentDidMount() {

    if (this.props.backgroundColor) {
      this.setState({
        btnBgColor: this.props.backgroundColor
      })
    }
  }
  render() {
    return (
      <View style={[style.MainVewStyle, {

        marginTop: this.props.marginTop, width: this.props.width, height:
          this.props.height, backgroundColor: this.props.backgroundColor,
        marginRight: this.props.marginRight, marginLeft: this.props.marginLeft,
        paddingHorizontal: this.props.paddingHorizontal,
        flex: this.props.flex,
        padding: this.props.padding, paddingVertical: this.props.paddingVertical,
        borderRadius: this.props.borderRadius,
      }, this.props.style]}
        elevation={this.props.elevation}
      >
        {
          <Text style={{
            fontFamily: this.props.fontFamily,
            fontSize: this.props.fontSize,
            textAlign: this.props.textAlign,
            alignSelf: this.props.alignSelf,
            color: this.props.color
            , flex: this.props.flex,
            textDecorationLine: this.props.textDecorationLine
          }}
            numberOfLines={this.props.numberOfLines}
            onPress={this.props.onPress}
          >
            {this.props.title}
          </Text>
        }
      </View>
    );
  }

}

// fontFamily:this.props.fontFamily,
const style = {
  MainVewStyle:
  {

  }
}
export default CommonText;
