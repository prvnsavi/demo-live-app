export const white = '#ffffff';
export const transparent = '#00000000';
export const transparentWhite = '#ffffff30';
export const transparentBlueSelector = '#1643B990';

export const colorPrimary = '#325b84';
export const colorPrimaryDark = '#325b84';
export const colorAccent = '#001428';

export const colorPrimarySelector = '#007EAE';
export const placeholderTextColorDark = '#151515';
export const placeholderTextColorLight = '#707070';
export const placeholderTextColorExtraLight = '#EBE9E8';

export const colorLightGrey = '#F6F6F6';
export const colorWhite = '#FFFFFF';
export const colorBlack = '#151515';
export const colorBlue = '#1643B9';
export const colorRed = '#D80004';
export const colorRedTrans = 'rgba(216, 0, 4, 0.5)';

// red usefull
export const colorRedd = '#D80004';
export const colorYellow = '#FFB44F';
export const colorButtonBlue = '#007FAF';
export const colorButtonOrage = '#F76224';
export const colorSwitchOffsetGreen = '#C8E4D4';
export const colorSwitchButtonGreen = '#00A352';
export const colorPendingRound = '#FEE6E8';
export const colorUpcomingRound = '#E4F4EA';

