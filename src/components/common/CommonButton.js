import React, { Component } from 'react';
import { Button, TouchableHighlight, View, Text } from 'react-native';
import * as Dimens from './Dimens';
import * as Colors from './Colors';
import CommonText from './CommonText';

//var width = Dimensions.get('window').width;

class CommonButton extends Component {
  state = { btnBgColor: Colors.blue };

  async componentDidMount() {
    if (this.props.backgroundColor) {
      this.setState({
        btnBgColor: this.props.backgroundColor
      })
    }
  }
  render() {
    return (
      (this.props.visible != undefined && !this.props.visible) ? null : (
        <View style={[style.MainVewStyle,
        {
          marginTop: this.props.marginTop,
          marginLeft: this.props.marginLeft,
          marginRight: this.props.marginRight,
          marginBottom: this.props.marginBottom,
          borderRadius: this.props.borderRadius,
          flex: this.props.flex,
          margin: this.props.margin,
          elevation: this.props.elevation
        }]}
          keyboardShouldPersistTaps={'always'}
        >
          {
            <TouchableHighlight
              activeOpacity={1}
              underlayColor={this.props.underlayColor}
              style={[style.BtnStyle, {
                borderRadius: this.props.borderRadius,
                backgroundColor: (this.props.backgroundColor != undefined ?
                  this.props.backgroundColor : Colors.colorPrimary),
              }]}
              onPress={this.props.onPress}
            >
              <CommonText
                width={this.props.width}
                textAlign={'center'}
                fontFamily={this.props.fontFamily}
                title={this.props.title}
                fontSize={this.props.fontSize}
                color={(this.props.textColorBtn != undefined) ?
                  this.props.textColorBtn : Colors.white}
              >{this.props.title}</CommonText>
            </TouchableHighlight>

          }
        </View>

      ));
  }

}




const style = {
  BtnStyle:
  {
    alignSelf: 'stretch',
    padding: Dimens.px_15,

  },
  MainVewStyle:
  {
    alignItems: 'center',
    justifyContent: 'center',
    width: '60%',
  }
}
export default CommonButton;
