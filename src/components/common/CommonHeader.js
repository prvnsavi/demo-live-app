import React, { Component } from 'react';
import { TouchableOpacity, View, Image, Text } from 'react-native';
import * as Dimens from './Dimens';
import * as Strings from './Strings';
import * as Colors from './Colors';
import CommonText from './CommonText';
import BaseComponent from './BaseComponent';


//var width = Dimensions.get('window').width;

class CommonHeader extends BaseComponent {
  state = {
    btnBgColor: 'transparent'
  };

  async componentDidMount() {
    if (this.props.backgroundColor) {
      this.setState({
        btnBgColor: this.props.backgroundColor
      })
    }
  }
  render() {
    return (
      <View style={{
        height: 50,
        alignSelf: this.props.alignSelf,
        flexDirection: 'row',
        backgroundColor: this.props.headerBg
      }}>
        <View style={style.LeftImageStye}>
          <TouchableOpacity style={[{ backgroundColor: this.state.btnBgColor }]}
            onPress={this.props.leftIconPress}
            activeOpacity={.8}
          >
            {this.props.leftICImagePath ?
              <Image source={this.props.leftICImagePath}
                style={style.IconStyle}
                tintColor={'white'}
                resizeMode={'center'}
              />
              : null}
          </TouchableOpacity>
        </View>
        <View style={style.HeaderTitleStyle}>
          <CommonText
            textAlign={this.props.textAlign}
            fontFamily={Strings.font_medium}
            title={this.props.headerTitle}
            fontSize={(this.props.fontSizeHTitle) ? (this.props.fontSizeHTitle) : 18}
            color={Colors.white}
          />
        </View>
        <View style={style.RightImageStye}>
          <TouchableOpacity style={[{ backgroundColor: this.state.btnBgColor }]}
            onPress={this.props.rightIconPress}
            activeOpacity={.8}
          >
            {this.props.rightICImagePath ?

              <View style={{
                flexDirection: 'row',
                alignItems: 'center',
                justifyContent: 'center'
              }}>

                <Image source={this.props.rightICImagePath}
                  style={{
                    width: Dimens.px_20,
                    height: Dimens.px_20,
                    marginLeft: Dimens.px_2,
                    marginBottom: Dimens.px_5,
                    padding: Dimens.px_5
                  }}
                  resizeMode={'center'}
                />
              </View>
              : null}
          </TouchableOpacity>
        </View>
      </View>
    );
  }

}
const style = {

  HeaderTitleStyle:
  {
    flexDirection: 'row',
    flex: 1,
    alignItems: 'center',
    justifyContent: 'center'
  },
  IconStyle:
  {
    width: Dimens.px_30,
    height: Dimens.px_30,
  },
  LeftImageStye:
  {
    alignSelf: 'flex-start',
    padding: Dimens.px_10
  }
  ,
  RightImageStye:
  {
    alignSelf: 'flex-end',
    padding: Dimens.px_10,

  }
  ,
  DeviderStyle:
  {
    backgroundColor: Colors.white,
    width: Dimens.devider_h_1,
    marginLeft: Dimens.px_5,
    marginRight: Dimens.px_5
  },
  DeviderImagStyle:
  {
    flexDirection: 'row',
  }
}

export default CommonHeader;
