import React, { Component } from 'react';
import { View, TextInput, Image } from 'react-native';
import * as Dimens from './Dimens';
import * as Colors from './Colors';
//var width = Dimensions.get('window').width;
class CommonTextInput extends Component {
  state = { btnBgColor: Colors.blue, editable: true };
  async componentDidMount() {
    if (this.props.backgroundColor) {
      this.setState({
        btnBgColor: this.props.backgroundColor
      })
    }
    if (!this.props.editable) {
      this.setState({ editable: this.props.editable })
    }
  }
  render() {
    return (
      <View style={[style.MainVewStyle,
      {
        marginTop: this.props.marginTop,
        backgroundColor: this.props.backgroundColor,
        borderBottomColor: (this.props.borderBottomColor) ?
          this.props.borderBottomColor : Colors.white
      }, this.props.style]}>
        {this.props.leftImagePath ?
          <Image source={this.props.leftImagePath}
            style={style.IconStyle}
          />
          : null}
        <TextInput style={[{
          fontFamily: this.props.fontFamily,
          paddingHorizontal: Dimens.px_10,
          color: (this.props.color != undefined) ? this.props.color : Colors.white,
          fontSize: this.props.fontSize,
          flex: 1,
          minHeight: this.props.minHeight,
          maxHeight: this.props.maxHeight,
          textAlignVertical: this.props.textAlignVertical,

        }]}
          blurOnSubmit={this.props.blurOnSubmit}
          secureTextEntry={this.props.secure}
          autoCapitalize={this.props.autoCapitalize}
          maxLength={this.props.maxLength}
          editable={this.state.editable != undefined ? this.state.editable : true}
          placeholder={this.props.placeholder}
          placeholderTextColor={this.props.placeholderTextColor}
          keyboardType={this.props.keyboardType}
          onChangeText={this.props.onChangeText}
          ref={this.props.refValue}
          returnKeyType={this.props.returnKeyType}
          onSubmitEditing={this.props.onSubmitEditing}
          value={'' + (this.props.value && this.props.value !== undefined) ?
            this.props.value : ''}
          multiline={this.props.multiline}
          tintColor={(this.props.selectionColor != undefined) ?
            this.props.selectionColor : Colors.white}
          selectionColor={(this.props.selectionColor != undefined) ?
            this.props.selectionColor : Colors.white}
          numberOfLines={this.props.numberOfLines}
        >
        </TextInput>
        {this.props.rightImagePath ?
          <Image source={this.props.rightImagePath}
            style={style.IconStyle}
          />
          : null}
      </View>
    );
  }


}

const style = {
  MainVewStyle:
  {
    flexDirection: 'row',
    backgroundColor: Colors.white,
    alignItems: 'center',
    justifyContent: 'flex-start',
    borderBottomColor: Colors.white,
    borderBottomWidth: Dimens.devider_h,
    minHeight: Dimens.minHeight,
  },
  IconStyle:
  {
    resizeMode: 'contain'
  }
}
export default CommonTextInput;
