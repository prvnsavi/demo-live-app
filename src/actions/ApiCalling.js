import * as ApiUrls from './ApiUrls';
import * as types from './types';
import * as Constants from '../utils/Constants';
import { Platform } from 'react-native';
import NetInfo from "@react-native-community/netinfo";
import Toast from 'react-native-simple-toast';
var METHOD_TYPE_POST = 'post';
var METHOD_TYPE_GET = 'get';


// Demo Live App project calling api start from here


export const getAlmubListApi = (data, screenName) => async (dispatch) => {
  callApi(data, dispatch, ApiUrls.URL_ALMUB_LIST,
    types.API_ALBUM_LIST, METHOD_TYPE_GET, screenName, true);
}

//end from here
export const callApi = async (data, dispatch, apiUrl, typeValue,
  methodType, screenName, showPBar) => {

  try {
    NetInfo.isConnected.fetch().then(isConnected => {
      if (Platform.OS === Constants.OS_TYPE_ANDROID)
        callApiAfterNetChecking(data, dispatch, apiUrl, typeValue,
          methodType, screenName, isConnected, showPBar);
    });

    function handleFirstConnectivityChange(isConnected) {
      if (Platform.OS === Constants.OS_TYPE_IOS)
        callApiAfterNetChecking(data, dispatch, apiUrl, typeValue,
          methodType, screenName, isConnected, showPBar);
      //console.log('Then, is ' + (isConnected ? 'online' : 'offline'));
      NetInfo.isConnected.removeEventListener(
        'connectionChange',
        handleFirstConnectivityChange
      );
    }
    NetInfo.isConnected.addEventListener(
      'connectionChange',
      handleFirstConnectivityChange
    );

  } catch (e) {
    console.log('exception', e);
  }
}

export const callApiAfterNetChecking = async (data, dispatch, apiUrl,
  typeValue, methodType, screenName, isConnected, showPBar) => {
  if (isConnected) {
    callApiFinal(data, dispatch, apiUrl, typeValue, methodType, screenName, showPBar);
  } else {
    setTimeout(() => { Toast.show('Network is not available', Toast.CENTER) }, 200)
    //alert('Network is not available');
  }
}
export const reqGetPaytmData = (data, screenName) => async (dispatch) => {
  var api = ApiUrls.API_PAYTM_RESPONSE + '?' + Constants.KEY_APPOINTMENT_ID + '=' + data[Constants.KEY_APPOINTMENT_ID];
  callApi(data, dispatch, api, types.API_PAYTM_RESPONSE, METHOD_TYPE_GET, screenName, true);
}
export const callApiFinal = (data, dispatch, apiUrl, typeValue,
  methodType, screenName, showPBar) => {
  //console.log('requestData: ' + JSON.stringify(data) + ' API: ' + apiUrl);
  dispatch({
    type: 'reload', payload: {
      showBar: showPBar,
      type: typeValue, currentScreen: screenName
    }
  });
  if (methodType == METHOD_TYPE_POST) {
    const formData = getFormDataFromObject(data);
    fetch(apiUrl, {
      method: methodType,
      body: formData
    }).then(response => response.text())
      .then(res => isValidResponse(res, dispatch, typeValue, screenName)).catch(error => {
        dispatch({ type: 'reload', payload: { showBar: false, type: typeValue, currentScreen: screenName } });
        setTimeout(() => {
          //setTimeout(() => { Toast.show(error, Toast.CENTER) }, 200)
          alert(error);
        }, 200)
      })
  }
  else if (methodType == METHOD_TYPE_GET) {
    fetch(apiUrl, {
      method: methodType
    }).then(response => response.text())
      .then(res => isValidResponse(res, dispatch, typeValue, screenName)).catch(error => {
        dispatch({ type: 'reload', payload: { showBar: false, type: typeValue, currentScreen: screenName } });
        setTimeout(() => {
          //setTimeout(() => { Toast.show(error, Toast.CENTER) }, 200)
          alert(error);
        }, 200)
      })
  }
}
export const objectToFormData = (obj, form, namespace) => {

  var fd = form || new FormData();
  var formKey;
  alert(formKey)

  for (var property in obj) {
    if (obj.hasOwnProperty(property)) {

      if (namespace) {
        formKey = namespace + '[' + property + ']';
      } else {
        formKey = property;
      }
      // if the property is an object, but not a File,
      // use recursivity.
      if (typeof obj[property] === 'object' && !(obj[property] instanceof File)) {
        objectToFormData(obj[property], fd, property);
      } else {
        // if it's a string or a File object
        fd.append(formKey, obj[property]);
      }
    }
  }
  return fd;
}

export const getFormDataFromObject = (data) => {
  const formData = new FormData();
  for (var key in data) {
    if (typeof data[key] === 'object') {
      var dataValue = data[key];
      if (key == Constants.KEY_IMAGES) {
        formData.append('image', data.image);
      }
      if (key == Constants.KEY_DOCUMENTS_WORK) {
        formData.append('work_experience_certificate', data.work_experience_certificate);
      }
      if (key == Constants.KEY_DOCUMENTS_ID) {
        formData.append('national_id_image', data.national_id_image);
      }
      else {
        if (dataValue !== null && dataValue.uri !== undefined && dataValue.uri !== null) {
        }
        else {
          if (dataValue != null) {
            dataValue = ((JSON.stringify(dataValue)));
            dataValue = dataValue.replace(/\\/g, '');
          }
        }
        if (dataValue != undefined && dataValue != null) {
          formData.append(key, dataValue);
        }
      }
    }
    else {
      if (data[key] != undefined && data[key] != null) {
        formData.append(key, data[key]);
      }
    }
  }
  return formData;
}

export const isValidResponse = (res, dispatch, apiType, screenName) => {

  var isValid = true;
  var msg = '';
  var response = undefined;

  if (res != undefined &&
    res != null &&
    res != '') {
    try {

      response = JSON.parse(res);
    }
    catch (exception) {
      msg = res;
    }
  }

  if (response != undefined &&
    response != null &&
    response != '') {
    dispatch({ type: apiType, payload: { response: response, type: apiType, currentScreen: screenName } });
  }
  return isValid;
}
