import React, { Component } from 'react';

import {
  Easing, Animated, SafeAreaView, Platform
} from 'react-native';

import * as  Colors from './src/components/common/Colors';
import * as Constants from './src/utils/Constants';
import AlbumList from './src/components/screens/AlbumList';
import FavoriteList from './src/components/screens/FavoriteList';
import { createStackNavigator, createAppContainer } from 'react-navigation';
import { Provider } from 'react-redux';
import store from './store';

export default class App extends React.Component {

  render() {

    const AppContainer = createAppContainer(AppStackNavigator)
    return (
      <Provider store={store} >
        <SafeAreaView style={style.container}>
          <AppContainer />
        </SafeAreaView>
      </Provider >
    );
  }
}
//drawerLockMode?: 'unlocked' | 'locked-close' | 'locked-open',

const AppStackNavigator = createStackNavigator({
  [Constants.SCREEN_ALBUM_LIST]: {
    screen: AlbumList, navigationOptions: {
      header: null,
      drawerLockMode: "locked-closed",
    }
  },
  [Constants.SCREEN_FAVORITE_LIST]: {
    screen: FavoriteList, navigationOptions: {
      header: null,
      drawerLockMode: "locked-closed",
    }
  },
},
  {
    transitionConfig: () => ({
      transitionSpec: {
        duration: 300,
        easing: Easing.out(Easing.poly(4)),
        timing: Animated.timing,
      },
      screenInterpolator: sceneProps => {
        const { layout, position, scene } = sceneProps;
        const { index } = scene;
        const width = layout.initWidth;
        const translateX = position.interpolate({
          inputRange: [index - 1, index, index + 1],
          outputRange: [width, 0, 0],
        });
        const opacity = position.interpolate({
          inputRange: [index - 1, index - 0.99, index],
          outputRange: [0, 1, 1],
        });
        return { opacity, transform: [{ translateX: translateX }] };
      },
    })
  })

AppStackNavigator.navigationOptions = ({ navigation }) => {
  let drawerLockMode = 'locked-closed';

  console.log('screen index0', navigation.state.routes);
  if (navigation.state.routes != null &&
    navigation.state.routes != undefined) {

    if (navigation.state.index >= 0) {
      drawerLockMode = 'locked-closed';
      disableGestures = false;
    }
    return {
      drawerLockMode,
    };
  }
};

const style = {
  container:
  {
    flexDirection: 'column',
    flex: 1,
    backgroundColor: Colors.colorPrimary,
    paddingTop: (Platform.OS === Constants.OS_TYPE_IOS) ? 20 : 0,
  }

}
